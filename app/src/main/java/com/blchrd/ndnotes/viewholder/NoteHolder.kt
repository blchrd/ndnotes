package com.blchrd.ndnotes.viewholder

import android.content.SharedPreferences
import android.view.View
import android.widget.ImageButton
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.isVisible
import androidx.preference.PreferenceManager
import androidx.recyclerview.widget.RecyclerView
import com.blchrd.ndnotes.R
import com.blchrd.ndnotes.entities.Note

class NoteHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    private val titleText: TextView = itemView.findViewById(R.id.note_list_item_title)
    private val contentText: TextView = itemView.findViewById(R.id.note_list_item_content)
    private val titleCircle: TextView = itemView.findViewById(R.id.note_list_item_header)
    private val pinnedButton: ImageButton = itemView.findViewById(R.id.note_list_item_pinned)
    private val preferences: SharedPreferences =
        PreferenceManager.getDefaultSharedPreferences(itemView.context)

    private var currentNote: Note? = null

    fun updateWithNote(note: Note) {
        currentNote = note
        titleText.text = note.title
        contentText.text = note.content
        if (!preferences.getBoolean(
                itemView.resources.getString(R.string.setting_key_display_pinned_indicator_in_list),
                false
            )
        ) {
            pinnedButton.isVisible = false
        } else {
            pinnedButton.isVisible = true
            if (note.pinned == true) {
                pinnedButton.setImageDrawable(
                    ResourcesCompat.getDrawable(
                        itemView.resources,
                        android.R.drawable.btn_star_big_on,
                        null
                    )
                )
            } else {
                pinnedButton.setImageDrawable(
                    ResourcesCompat.getDrawable(
                        itemView.resources,
                        android.R.drawable.btn_star_big_off,
                        null
                    )
                )
            }
        }
        if (note.title != "") {
            titleCircle.text = note.title!!.first().uppercase()
        }
    }
}