package com.blchrd.ndnotes

import android.annotation.SuppressLint
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.Window
import android.widget.ImageButton
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import androidx.preference.PreferenceManager
import com.blchrd.ndnotes.database.AppDatabase
import com.blchrd.ndnotes.entities.Note
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*
import kotlin.coroutines.CoroutineContext

class MainActivity : AppCompatActivity(), ActivityCallbackInterface, CoroutineScope {
    //TODO : dark theme (option is here, have to implement it)
    //TODO : choose files for backup / restore
    //TODO : show all note (archived and active) and add an indicator for archived note
    //TODO : handle the todo.txt files
    private var noteDatabase: AppDatabase? = null
    private var currentNote: Note? = null
    private var job: Job = Job()
    private var preferences: SharedPreferences? = null
    private var isNotePinned: Boolean = false
    private var showArchived: Boolean = false

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + job

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_settings -> {
                val intent = Intent(this, SettingsActivity::class.java)
                startActivity(intent)
                true
            }
            else -> {
                super.onOptionsItemSelected(item)
            }
        }
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 0) {
            supportFragmentManager.popBackStack()
            val bottomNav = findViewById<BottomNavigationView>(R.id.main_bottom_nav)
            bottomNav.menu.clear()
            bottomNav.inflateMenu(R.menu.bottom_nav_menu_main)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        noteDatabase = AppDatabase.getDatabase(this)
        preferences = PreferenceManager.getDefaultSharedPreferences(this)
        showFragment(NoteListFragment(this))

        setBottomNavMenuListener()

        this.window.setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.activity_main)
    }

    @SuppressLint("SimpleDateFormat")
    private fun setBottomNavMenuListener() {
        val bottomNav = findViewById<BottomNavigationView>(R.id.main_bottom_nav)
        bottomNav.setOnItemSelectedListener { menuItem ->
            when (menuItem.itemId) {
                R.id.bottom_nav_menu_item_add -> {
                    currentNote = null
                    showFragment(NoteEntryFragment())
                }
                R.id.bottom_nav_menu_item_archived_toggle -> {
                    val fragment =
                        supportFragmentManager.findFragmentById(R.id.main_frame_layout) as NoteListFragment
                    fragment.toggleArchivedActiveNotes()
                    if (showArchived) {
                        menuItem.title = resources.getString(R.string.bottom_menu_archived_toggle)
                    } else {
                        menuItem.title = resources.getString(R.string.bottom_menu_restored_toggle)
                    }
                    showArchived = !showArchived
                }
                R.id.bottom_nav_menu_item_return -> {
                    showNoteListFragment()
                }
                R.id.bottom_nav_menu_item_archive -> {
                    launch {
                        if (currentNote != null) {
                            val formatedDate =
                                SimpleDateFormat(getString(R.string.app_format_date)).format(Date())
                            val formatedTime =
                                SimpleDateFormat(getString(R.string.app_format_time)).format(Date())
                            val dateTime = "$formatedDate $formatedTime"

                            currentNote!!.archived = true
                            currentNote!!.archivedAt = dateTime

                            noteDatabase!!.noteDao().update(currentNote!!)

                            showNoteListFragment()
                            onNoteArchived()
                        }
                    }
                }
                R.id.bottom_nav_menu_item_restore -> {
                    launch {
                        if (currentNote != null) {
                            currentNote!!.archived = false
                            currentNote!!.archivedAt = ""

                            noteDatabase!!.noteDao().update(currentNote!!)

                            showNoteListFragment()
                            onNoteRestored()
                        }
                    }
                }
                R.id.bottom_nav_menu_item_save -> {
                    launch {
                        val title = findViewById<TextView>(R.id.note_entry_title)
                        val content = findViewById<TextView>(R.id.note_entry_content)
                        val formatedDate =
                            SimpleDateFormat(getString(R.string.app_format_date)).format(Date())
                        val formatedTime =
                            SimpleDateFormat(getString(R.string.app_format_time)).format(Date())
                        val dateTime = "$formatedDate $formatedTime"

                        if (currentNote == null) {
                            val note = Note(
                                title = title.text.toString(),
                                content = content.text.toString(),
                                lastUpdate = dateTime,
                                pinned = isNotePinned
                            )
                            noteDatabase!!.noteDao().insert(note)
                        } else {
                            currentNote!!.title = title.text.toString()
                            currentNote!!.content = content.text.toString()
                            currentNote!!.lastUpdate = dateTime
                            currentNote!!.pinned = isNotePinned

                            noteDatabase!!.noteDao().update(currentNote!!)
                        }

                        onNoteUpdate()
                    }
                }
                R.id.bottom_nav_menu_item_delete -> {
                    confirmDeleteDialog()
                }
            }
            false
        }
    }

    //Activity callback functions
    override fun showFragment(fragment: Fragment, note: Note?): Fragment {
        val bottomNav = findViewById<BottomNavigationView>(R.id.main_bottom_nav)
        if (fragment::class.java == NoteListFragment::class.java) {
            supportFragmentManager.beginTransaction().replace(
                R.id.main_frame_layout, fragment,
                fragment.javaClass.simpleName
            ).commit()

            bottomNav.menu.clear()
            bottomNav.inflateMenu(R.menu.bottom_nav_menu_main)
        } else if (fragment::class.java == NoteEntryFragment::class.java) {
            supportFragmentManager.beginTransaction().replace(
                R.id.main_frame_layout, fragment,
                fragment.javaClass.simpleName
            ).addToBackStack(getString(R.string.backstack_key_note_list)).commit()

            bottomNav.menu.clear()
            bottomNav.inflateMenu(R.menu.bottom_nav_menu_note)
            val menu = bottomNav.menu
            val archiveItem = menu.findItem(R.id.bottom_nav_menu_item_archive)
            val restoreItem = menu.findItem(R.id.bottom_nav_menu_item_restore)
            val deleteItem = menu.findItem(R.id.bottom_nav_menu_item_delete)

            if (note != null) {
                currentNote = note
                isNotePinned = note.pinned!!

                if (note.archived == true) {
                    archiveItem.isVisible = false
                    restoreItem.isVisible = true
                } else {
                    archiveItem.isVisible = true
                    restoreItem.isVisible = false
                }
            } else {
                archiveItem.isVisible = false
                restoreItem.isVisible = false
                deleteItem.isVisible = false
            }
        }

        return fragment
    }

    override fun getPreference(key: String): String? {
        return preferences!!.getString(key, "")
    }

    //confirmation dialog
    private fun confirmDeleteDialog() {
        val alertDialog: AlertDialog.Builder = AlertDialog.Builder(this@MainActivity)
        alertDialog.setTitle(getString(R.string.app_string_delete_note_title))
        alertDialog.setMessage(getString(R.string.app_string_delete_note_message))
        alertDialog.setPositiveButton(
            getString(R.string.app_string_button_yes)
        ) { _, _ ->
            launch {
                if (currentNote != null) {
                    noteDatabase!!.noteDao().delete(currentNote!!)
                }

                onNoteDeletion()
            }
        }
        alertDialog.setNegativeButton(
            getString(R.string.app_string_button_no)
        ) { _, _ -> }

        val alert: AlertDialog = alertDialog.create()
        alert.setCanceledOnTouchOutside(false)
        alert.show()
    }

    private fun showNoteListFragment() {
        if (supportFragmentManager.backStackEntryCount > 0) {
            supportFragmentManager.popBackStack()
            val bottomNav = findViewById<BottomNavigationView>(R.id.main_bottom_nav)
            bottomNav.menu.clear()
            bottomNav.inflateMenu(R.menu.bottom_nav_menu_main)
        } else {
            showFragment(NoteListFragment(this))
        }
    }

    fun onToggleStar(view: View) {
        val favButton = view.findViewById<ImageButton>(R.id.note_entry_pinned)
        if (isNotePinned) {
            favButton.setImageDrawable(
                ResourcesCompat.getDrawable(
                    resources,
                    android.R.drawable.btn_star_big_off,
                    null
                )
            )
        } else {
            favButton.setImageDrawable(
                ResourcesCompat.getDrawable(
                    resources,
                    android.R.drawable.btn_star_big_on,
                    null
                )
            )
        }
        isNotePinned = !isNotePinned
    }

    //Callback for coroutine
    private fun onNoteDeletion() {
        Toast.makeText(this, getString(R.string.app_string_toast_note_deleted), Toast.LENGTH_SHORT)
            .show()
        showNoteListFragment()
    }

    private fun onNoteUpdate() {
        Toast.makeText(this, getString(R.string.app_string_toast_note_saved), Toast.LENGTH_SHORT)
            .show()
        if (preferences!!.getBoolean(
                getString(R.string.setting_key_close_after_saving),
                false
            )
        ) {
            showNoteListFragment()
        }
    }

    private fun onNoteArchived() {
        Toast.makeText(this, getString(R.string.app_string_toast_note_archived), Toast.LENGTH_SHORT)
            .show()
    }

    private fun onNoteRestored() {
        Toast.makeText(this, getString(R.string.app_string_toast_note_restored), Toast.LENGTH_SHORT)
            .show()
    }
}