package com.blchrd.ndnotes

import androidx.fragment.app.Fragment
import com.blchrd.ndnotes.entities.Note

interface ActivityCallbackInterface {
    fun showFragment(fragment: Fragment, note: Note? = null) : Fragment

    fun getPreference(key: String) : String?
}