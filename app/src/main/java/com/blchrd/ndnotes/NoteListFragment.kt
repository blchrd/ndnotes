package com.blchrd.ndnotes

import android.os.Bundle
import android.view.*
import android.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.blchrd.ndnotes.adapter.NoteRecyclerAdapter
import com.blchrd.ndnotes.database.AppDatabase
import com.blchrd.ndnotes.entities.Note
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class NoteListFragment(
    private val listener: ActivityCallbackInterface? = null,
) : Fragment(), CoroutineScope, SearchView.OnQueryTextListener, MenuItem.OnActionExpandListener {
    private var job: Job = Job()
    private var adapter: NoteRecyclerAdapter? = null
    private var showArchived: Boolean = false

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + job

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.title_bar, menu)

        //listener for search View
        val item = menu.findItem(R.id.search_title)
        val searchView = item.actionView as SearchView

        searchView.setOnQueryTextListener(this)

        item.setOnActionExpandListener(this)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_archived_note -> {
                showArchived = true
                launch { loadNotes(view) }
                true
            }
            R.id.action_active_note -> {
                showArchived = false
                launch { loadNotes(view) }
                true
            }
            else -> {
                super.onOptionsItemSelected(item)
            }
        }
    }

    //Override for listener
    override fun onQueryTextSubmit(query: String?): Boolean {
        adapter!!.filter.filter(query)

        return true
    }

    override fun onQueryTextChange(query: String?): Boolean {
        adapter!!.filter.filter(query)

        return true
    }

    override fun onMenuItemActionCollapse(item: MenuItem?): Boolean {
        adapter!!.filter.filter("")
        return true
    }

    override fun onMenuItemActionExpand(item: MenuItem?): Boolean {
        return true
    }

    override fun onResume() {
        super.onResume()
        launch {
            loadNotes(view)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_note_list, container, false)
        setHasOptionsMenu(true)

        return view
    }

    fun toggleArchivedActiveNotes() {
        showArchived = !showArchived
        launch { loadNotes(view) }
    }

    private suspend fun loadNotes(view: View?) {
        val noteDatabase = activity?.let { AppDatabase.getDatabase(it.applicationContext) }
        val recycler = view?.findViewById<RecyclerView>(R.id.note_list_recycler)

        //0 by date desc, 1 alphabetically
        var items: ArrayList<Note> = ArrayList()
        val sortPref = listener!!.getPreference(getString(R.string.setting_key_sort_type))
        if (!showArchived) {
            if (sortPref == getString(R.string.value_sort_alphabetically) || sortPref == "") {
                items = ArrayList(noteDatabase!!.noteDao().getAllActive())
            } else if (sortPref == getString(R.string.value_sort_by_date_desc)) {
                items = ArrayList(noteDatabase!!.noteDao().getAllAlphabetizedActive())
            }
        } else {
            if (sortPref == getString(R.string.value_sort_alphabetically) || sortPref == "") {
                items = ArrayList(noteDatabase!!.noteDao().getAllArchived())
            } else if (sortPref == getString(R.string.value_sort_by_date_desc)) {
                items = ArrayList(noteDatabase!!.noteDao().getAllAlphabetizedArchived())
            }
        }

        adapter = NoteRecyclerAdapter(items, listener)

        recycler!!.layoutManager = LinearLayoutManager(activity)
        recycler.adapter = adapter
    }
}