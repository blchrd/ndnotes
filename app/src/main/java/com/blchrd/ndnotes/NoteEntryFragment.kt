package com.blchrd.ndnotes

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import com.blchrd.ndnotes.entities.Note

class NoteEntryFragment(private val item: Note? = null) : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_note_entry, container, false)
        val title = view.findViewById<TextView>(R.id.note_entry_title)
        val content = view.findViewById<TextView>(R.id.note_entry_content)
        val date = view.findViewById<TextView>(R.id.note_entry_date)
        val pinned = view.findViewById<ImageButton>(R.id.note_entry_pinned)

        if (item != null) {
            title.text = item.title
            content.text = item.content
            date.text = String.format(getString(R.string.note_entry_last_update,item.lastUpdate))
            if (item.pinned!!) {
                pinned.setImageDrawable(ResourcesCompat.getDrawable(
                    resources,
                    android.R.drawable.btn_star_big_on,
                    null
                ))
            } else {
                pinned.setImageDrawable(ResourcesCompat.getDrawable(
                    resources,
                    android.R.drawable.btn_star_big_off,
                    null
                ))
            }
        }

        return view
    }
}