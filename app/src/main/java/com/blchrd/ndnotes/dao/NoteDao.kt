package com.blchrd.ndnotes.dao

import androidx.room.*
import com.blchrd.ndnotes.entities.Note

@Dao
interface NoteDao {
    @Query("SELECT * FROM ndNotesDb")
    suspend fun getAll(): List<Note>

    @Query("SELECT * FROM ndNotesDb WHERE archived = 0 ORDER BY pinned DESC, lastupdate DESC")
    suspend fun getAllActive(): List<Note>

    @Query("SELECT * FROM ndNotesDb WHERE archived = 1 ORDER BY pinned DESC, lastupdate DESC")
    suspend fun getAllArchived(): List<Note>

    @Query("SELECT * FROM ndNotesDb WHERE archived = 0 ORDER BY pinned DESC, title ASC")
    suspend fun getAllAlphabetizedActive(): List<Note>

    @Query("SELECT * FROM ndNotesDb WHERE archived = 1 ORDER BY pinned DESC, title ASC")
    suspend fun getAllAlphabetizedArchived(): List<Note>

    @Query("SELECT * FROM ndNotesDb WHERE uid = :uid")
    suspend fun loadById(uid: Int): Note

    @Update
    suspend fun update(note: Note)

    @Insert
    suspend fun insert(note: Note)

    @Delete
    suspend fun delete(note: Note)
}