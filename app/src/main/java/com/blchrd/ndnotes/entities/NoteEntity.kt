package com.blchrd.ndnotes.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "ndNotesDb")
data class Note(
    @PrimaryKey(autoGenerate = true) var uid: Int = 0,
    @ColumnInfo(name = "title") var title: String?,
    @ColumnInfo(name = "content") var content: String?,
    @ColumnInfo(name = "lastupdate") var lastUpdate: String?,
    @ColumnInfo(name = "pinned") var pinned: Boolean? = false,
    @ColumnInfo(name = "archived") var archived: Boolean? = false,
    @ColumnInfo(name = "archivedat") var archivedAt: String? = ""
)
