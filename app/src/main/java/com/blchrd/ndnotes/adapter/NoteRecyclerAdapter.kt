package com.blchrd.ndnotes.adapter

import android.content.SharedPreferences
import android.content.res.Resources
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.preference.PreferenceManager
import androidx.recyclerview.widget.RecyclerView
import com.blchrd.ndnotes.ActivityCallbackInterface
import com.blchrd.ndnotes.NoteEntryFragment
import com.blchrd.ndnotes.R
import com.blchrd.ndnotes.entities.Note
import com.blchrd.ndnotes.viewholder.NoteHolder

class NoteRecyclerAdapter(
    private var fullList: ArrayList<Note>,
    private val listener: ActivityCallbackInterface? = null
) : RecyclerView.Adapter<NoteHolder>(), Filterable {
    private var preferences: SharedPreferences? = null
    private var resources: Resources? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NoteHolder {
        val noteItem = LayoutInflater.from(parent.context).inflate(
            R.layout.note_item,
            parent,
            false
        )

        resources = parent.resources
        preferences = PreferenceManager.getDefaultSharedPreferences(parent.context)

        return NoteHolder(noteItem)
    }

    private var dataSet: ArrayList<Note> = fullList

    override fun onBindViewHolder(holder: NoteHolder, position: Int) {
        val note = dataSet[position]
        holder.updateWithNote(note)

        holder.itemView.setOnClickListener {
            listener!!.showFragment(NoteEntryFragment(note), note)
        }
    }

    override fun getItemCount(): Int {
        return dataSet.size
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val charSearch = constraint.toString()
                dataSet = if (charSearch.isEmpty()) {
                    fullList
                } else {
                    val resultList = ArrayList<Note>()
                    for (note in dataSet) {
                        if (note.title!!.lowercase().contains(constraint.toString().lowercase())) {
                            resultList.add(note)
                        }
                        if (preferences!!.getBoolean(
                                resources!!.getString(R.string.setting_key_search_in_content),
                                false
                            )
                        ) {
                            if (note.content!!.lowercase()
                                    .contains(constraint.toString().lowercase())
                            ) {
                                resultList.add(note)
                            }
                        }
                    }
                    resultList
                }

                val filterResults = FilterResults()
                filterResults.values = dataSet
                return filterResults
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                dataSet = results?.values as ArrayList<Note>
                notifyDataSetChanged()
            }
        }
    }
}