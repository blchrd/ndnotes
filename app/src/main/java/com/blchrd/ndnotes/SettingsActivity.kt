package com.blchrd.ndnotes

import android.content.Context
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import com.blchrd.ndnotes.database.AppDatabase
import com.blchrd.ndnotes.entities.Note
import com.google.gson.Gson
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import org.json.JSONArray
import java.io.File
import kotlin.coroutines.CoroutineContext

class SettingsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.settings_activity)
        if (savedInstanceState == null) {
            supportFragmentManager
                .beginTransaction()
                .replace(R.id.settings_frame_layout, SettingsFragment())
                .commit()
        }

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    class SettingsFragment : PreferenceFragmentCompat(), CoroutineScope {
        private val path = context?.filesDir
        private val fileName = "backup.txt"
        private val filePath = "$path/$fileName"
        private val gson = Gson()
        private var job: Job = Job()
        override val coroutineContext: CoroutineContext
            get() = Dispatchers.Main + job
        private var noteDatabase: AppDatabase? = null

        override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
            if (context is Context) {
                noteDatabase = AppDatabase.getDatabase(context as Context)
            }

            setPreferencesFromResource(R.xml.root_preferences, rootKey)
            val backupButton =
                findPreference<Preference>(getString(R.string.setting_key_backup_button))
            val restoreButton =
                findPreference<Preference>(getString(R.string.setting_key_restore_button))

            backupButton?.setOnPreferenceClickListener {
                launch {
                    backupNotes()
                }
                return@setOnPreferenceClickListener true
            }

            restoreButton?.setOnPreferenceClickListener {
                launch {
                    restoreNotes()
                }
                return@setOnPreferenceClickListener true
            }
        }

        private suspend fun backupNotes() {
            val noteList = noteDatabase!!.noteDao().getAll()
            val jsonString = gson.toJson(noteList)

            File(path, fileName).printWriter().use { out ->
                out.println(jsonString)
            }

            toast(getString(R.string.app_string_toast_notes_backuped, filePath))
        }

        private suspend fun restoreNotes() {
            File(path, fileName).bufferedReader().use { out ->
                val list: ArrayList<Note> = ArrayList()
                val jsonString = out.readText()
                val jsonArray = JSONArray(jsonString)
                for (i in 0 until jsonArray.length()) {
                    list.add(gson.fromJson(jsonArray[i].toString(), Note::class.java))
                }

                for (note in list) {
                    note.uid = 0
                    noteDatabase!!.noteDao().insert(note)
                }
            }
            toast(getString(R.string.app_string_toast_notes_restored))
        }

        private fun toast(text: String) {
            Toast.makeText(context, text, Toast.LENGTH_SHORT).show()
        }
    }
}